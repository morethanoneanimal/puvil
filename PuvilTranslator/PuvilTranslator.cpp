#include <iostream>
#include "puvil.h"
#include "BufferedFile.h"
#include "EventReader.h"


int main(int argc, char** argv)
{	
	if(argc != 3)
	{
		std::cout << "Usage: PuvilTranslator.exe encryptedFile outFile" << std::endl;		
		return 1;
	}
	EventReader reader(argv[1]);
	BufferedFile writer(argv[2], 1024);
	Event eve;
	while(reader.ReadEvent(&eve))
		writer.Write(eve.toString());
	writer.Flush();

	return 0;
}



