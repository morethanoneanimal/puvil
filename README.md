# Puvil #

Tools for authenticating people by the keystroke dynamics.

Windows only.

I don't remember where did project name come from.

### What is done ###

* Tool which gathers data from keyboard and stores it in file (basicly - keylogger)
* Simple method for analyzing data

### To do ###

* Sophisticated, advanced method for data analysis

### Structure ###

* Puvil - library shared among projects, data structures for keystroke data are defined here as well as some utility classess (BufferedFile for example)
* PuvilAgent - collects data from keyboard and store them in file.
* PuvilAnalyze - analyzes data from PuvilAgent
* PuvilTranslator - converts data from PuvilAgent into human-friendly form