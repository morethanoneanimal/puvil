#ifndef TIMELIST_H
#define TIMELIST_H

#include <vector>

class TimeList
{
protected:	
	std::vector<float> vec;

public:

	void addValue(float v)
	{
		vec.push_back(v);
	}

	float getAvg() const
	{
		if (!getCount())
		{
			return 0.0f;
		}
		{
			float sum = 0.0f;
			for (float val : vec)
			{
				sum += val;
			}
			return sum / getCount();
		}
	}
	
	int getCount() const
	{
		return vec.size();
	}
	
	TimeList()
	{ }

	~TimeList()
	{ }



};


#endif