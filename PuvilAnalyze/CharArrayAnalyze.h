#ifndef CHARARRAYANALYZE_H
#define CHARARRAYANALYZE_H

#include "BaseAnalyzer.h"
#include "TimeList.h"
#include <sstream>

#define LETTERS_COUNT 26

class CharArrayAnalyzer : public BaseAnalyzer
{
protected:
	TimeList matrix[LETTERS_COUNT][LETTERS_COUNT];
	int prevCode;
	DWORD prevTime;
	int backspaces, chars;
public:
	CharArrayAnalyzer() : prevCode(-1), backspaces(0), chars(0)
	{}
	virtual void ConsumeEvent(Event* ev)
	{
		if (!ev->pressed) return;
		if (ev->key == VK_BACK)
		{
			++backspaces;
		}
		else
		{
			if (!ev->isChar())
				return;			
			++eventsProcessed;
			++chars;
			if (prevCode > -1)
			{
				DWORD time = ev->timestamp - prevTime;	
				if (time < 400)
					matrix[prevCode - 'A'][ev->key - 'A'].addValue(time);
			}			
			prevCode = ev->key;
			prevTime = ev->timestamp;
			
		}
	}

	virtual std::string getAnalyze()
	{
		std::stringstream analyze;
		analyze.setf(std::ios::fixed);
		analyze << "CharAnalyze" << std::endl;
		for (int i = 0; i < LETTERS_COUNT; ++i)
		{
			char leftLetter = i + 'A';
			for (int k = 0; k < LETTERS_COUNT; ++k)
			{
				char rightLetter = k + 'A';
				analyze << leftLetter << rightLetter << ": " << matrix[i][k].getAvg() << " (" << matrix[i][k].getCount() << ")" << std::endl;
			}
		}
		analyze << "error ration: " << ((float)backspaces / (float)chars);
		return analyze.str();
	}
};


#endif