#ifndef BASEANALYZER_H
#define BASEANALYZER_H

#include "event.h"
#include <string>

class BaseAnalyzer
{
protected:
	int eventsProcessed;

public:
	virtual void ConsumeEvent(Event* ev) = 0;

	BaseAnalyzer() : eventsProcessed(0)
	{ }

	int getEventsProcessedCount() const
	{
		return eventsProcessed;
	}

	virtual std::string getAnalyze() = 0;


};

#endif