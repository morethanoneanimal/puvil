#include <iostream>
#include "puvil.h"
#include "BufferedFile.h"
#include "EventReader.h"
#include "CharArrayAnalyze.h"

int main(int argc, char** argv)
{		
	if(argc != 2)
	{
		std::cout << "Usage: PuvilAnalyze.exe fileFullOfData" << std::endl;		
		return 1;
	}
	EventReader reader(argv[1]);	
	Event eve;
	long prev = 0;
	double avg = 0.0;
	long total = 0;

	CharArrayAnalyzer charArary;

	while(reader.ReadEvent(&eve))
	{	
		charArary.ConsumeEvent(&eve);
		//std::cout << eve.key << std::endl;
		if(eve.isChar() && eve.pressed)
		{
			if(prev == 0)
				prev = eve.timestamp;
			else
			{				
				if(eve.timestamp - prev < 1000)
				{
					//std::cout << eve.toString();
					avg += (eve.timestamp - prev);
					prev = eve.timestamp;
					++total;
					//std::cout << "actual: " << avg/total << std::endl;
				}
				prev = eve.timestamp;

			}
		}
	}
	std::cout.setf(std::ios::fixed);
	std::cout << "Zespol ekspertow ocenil sredni czas miedzy jednym klawiszem a drugim i wynosi on " << avg/total;
	
	BufferedFile output("analyze.txt", 100);
	output.Write(charArary.getAnalyze());
	std::stringstream avgStream;
	avgStream << std::endl << "Avg speed: " << (avg / total) << std::endl;
	output.Write(avgStream.str());
	output.Flush();

	char shit[2];
	std::cin >> shit;


	return 0;
}