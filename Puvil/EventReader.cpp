#include "EventReader.h"



	
void EventReader::OpenHandle()
{
	if(!hFile)
	{
		hFile = CreateFile(filePath, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);			
	}
}

EventReader::EventReader(LPCSTR _filePath): hFile(0), filePath(_filePath), eventsSoFar(0)
{
	OpenHandle();
}

bool EventReader::HasNext()
{
	return false;	
}

bool EventReader::ReadEvent(Event* e)
{
	if(e == 0)
		e = new Event();
	char buf[7];
	DWORD bytesRead;
	if(ReadFile(hFile, buf, 7, &bytesRead, 0) == TRUE)
	{
		if(bytesRead == 7)
		{
			e->deserialize(buf);
			return true;
		}
	}
	return false;
}


