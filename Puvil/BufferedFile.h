#ifndef BUFFERED_FILE_H
#define BUFFERED_FILE_H

#include <windows.h>
#include <string>

// 

class BufferedFile
{
private:
	HANDLE hFile;
	LPCSTR filePath;

	char* buffer;
	int bufferSize;
	int filled;

	void OpenHandle();
	
	void CloseHandleSafely();
	void BrutalWrite(const char *chunk, int size);
	

	// diagnostyczne
	int _dBrutWrite;
	int _dFlush;
	
public:
	BufferedFile(LPCSTR _filePath, int buffer_size);	
	~BufferedFile();
	
	void Flush();
	void Write(const std::string &str);

	// najbardziej zakr�cona funkcja, je�li chcesz co� zapisa� SZYBKO to prosisz o miejsce w pami�ci gdzie mo�esz pisa�
	// zwracany jest adres bufora, z przesuni�ciem odpowiednio do wype�nienia
	char* getSpaceToWrite(int size);
	int getBufferSize() const;
	// jak bardzo bufor jest zape�niony
	int spaceUsed() const;

};


#endif