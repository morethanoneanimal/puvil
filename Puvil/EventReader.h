#ifndef EVENTREADER_H
#define EVENTREADER_H

#include <windows.h>
#include "event.h"

class EventReader
{
protected:
	HANDLE hFile;
	LPCSTR filePath;
	int eventsSoFar;
	
	void OpenHandle();
public:
	EventReader(LPCSTR _filePath);

	bool HasNext();

	bool ReadEvent(Event* e);



};



#endif