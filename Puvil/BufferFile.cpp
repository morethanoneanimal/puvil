#include "BufferedFile.h"


void BufferedFile::OpenHandle()
{
	if(!hFile)
	{
		hFile = CreateFile(filePath, GENERIC_WRITE, FILE_SHARE_READ, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
		SetFilePointer(hFile,0,NULL,FILE_END);
	}
}
void BufferedFile::CloseHandleSafely()
{
	if(hFile) 
	{
		CloseHandle(hFile);
		hFile = 0;
	}
}


void BufferedFile::BrutalWrite(const char *chunk, int size)
{
	++_dBrutWrite;
	OpenHandle();
	DWORD fWritten;
	WriteFile(hFile, chunk, size, &fWritten, 0);
	CloseHandleSafely();
}




BufferedFile::BufferedFile(LPCSTR _filePath, int buffer_size) : hFile(0), filePath(_filePath), filled(0), _dBrutWrite(0), _dFlush(0), bufferSize(buffer_size)
{
	buffer = new char[bufferSize];
}
BufferedFile::~BufferedFile()
{
	Flush();			
	delete[] buffer;

}
void BufferedFile::Flush()
{	
	++_dFlush;
	BrutalWrite(buffer, filled);
	ZeroMemory(buffer, sizeof(char) * bufferSize);
	filled = 0;
}
void BufferedFile::Write(const std::string &str)
{
	if(str.size() > bufferSize)
	{
		Flush();
		BrutalWrite(str.c_str(), str.size());
	}
	else
	{
		if (str.size() + filled >= bufferSize)
			Flush();
		memcpy(buffer + filled, str.c_str(), str.size());
		filled += str.size();
	}
}



char* BufferedFile::getSpaceToWrite(int size)
{
	if(filled + size > bufferSize)
	{
		Flush();
	}
	char* pointer = &buffer[filled];
	filled += size;
	return pointer;
}

int BufferedFile::getBufferSize() const
{
	return bufferSize;
}
int BufferedFile::spaceUsed() const
{
	return filled;
}
