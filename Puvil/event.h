#ifndef EVENT_H
#define EVENT_H

#include <windows.h>
#include <sstream>

// Eventem jest press albo release dowolnego klawisza z klawiatury

class Event
{
private:
public:
	Event(): timestamp(0), pressed(false)
	{ }
	
	DWORD timestamp;	// w milisekundach	
	bool pressed;
	USHORT key;


	void serialize(char* dest)
	{		
		dest[0] = pressed;
		memcpy(&dest[1], &key, sizeof(USHORT));
		memcpy(&dest[3], &timestamp, sizeof(DWORD));
	}

	void deserialize(char* src)
	{
		pressed = (bool)src[0];
		memcpy(&key, &src[1], sizeof(USHORT));
		memcpy(&timestamp, &src[3], sizeof(DWORD));
	}

	// czy klawisz jest jak�� literk�
	bool isChar()
	{
		return key >= 65 && key <= 90;
	}

	std::string toString()
	{
		std::string msgStr;
		if(key == VK_SHIFT)
			msgStr = "#ShiftDown";
		else if(key == VK_CONTROL)
		{
			msgStr = "#CtrlDown";		
		}
		else if(key == VK_RETURN)
		{
			msgStr = "#Enter";
		}
		else if(key == VK_BACK)
		{
			msgStr = "#Backspace";
		}
		else if(key == VK_CAPITAL)
		{
			msgStr = "#CapsLock$";
		}
		else
		{
			msgStr = MapVirtualKey(key,MAPVK_VK_TO_CHAR);
		}
		std::stringstream stream;
		stream << timestamp << " " << msgStr << " " << pressed << std::endl;
		return stream.str();
	}

	// ile miejsca zajmuje zserializowany obiekt
	static int getSize()
	{
		return sizeof(DWORD) + sizeof(USHORT) + 1;
	}
};

#endif